<?php
return array(
	'uploadPath' => public_path() . '/static/',
	'publicPath' => 'static',
	'defaults' => array(
		'folder' => 'default',
		'thumbs' => array(
			array(640, 480, '-big'),
			array(267, 200, '-med'),
			array(133, 100, '-sml'),
		),
		'thumbsFolder' => 'thumbs',
		'main' => array(850, 650),
		'preserveThumbRatio' => false,
		'upsizeThumbs' => true
	)
);