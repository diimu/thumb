<?php namespace Diimu\Thumb;

use Intervention\Image\Image;

class ThumbInput extends Thumb
{
	protected $input;
	protected $config;

	public function __construct($config)
	{
		$this->config = $config;
	}

	public function addInput($input)
	{
		$this->input = $input;
	}

	protected function processImage($options)
	{
		$return = array();
		//retrieve input and folder
		$input = $this->input;

		//configs
		$folder = $options['folder'];
		$uploadPath = $this->getConfig('uploadPath');
		$preserveRatio = $options['preserveThumbRatio'];
		$upsizeThumbs = $options['upsizeThumbs'];
		$thumbsFolder = $options['thumbsFolder'];

		$mainWidth = $options['main'][0];
		$mainHeight = $options['main'][1];

		//move original to generated folder
		foreach($input as $in)
		{
			$ext = $in->getClientOriginalExtension();
			$name = \Illuminate\Support\Str::random(4);
			$nameFull = $name . '.' . $ext;

			$imageFolder = $uploadPath . $folder;
			$in->move($imageFolder, $in->getClientOriginalName());

			$movedPath = $uploadPath . $folder . '/' . $in->getClientOriginalName();

			//this creates the dir if not exist (thumbs dir)
			$this->createDirIfNotExists($imageFolder . '/' . $thumbsFolder);

			$image = array();
			foreach($options['thumbs'] as $thumb)
			{
				//thumbs config by array
				$width = $thumb[0];
				$height = $thumb[1];
				$postfix = $thumb[2];

				$thumbName = $name . $postfix . '.' . $ext;
				$thumbPath = $imageFolder . '/' . $thumbsFolder . '/' . $thumbName;
				
				Image::make($movedPath)
					->resize($width, $height, $preserveRatio, $upsizeThumbs)
					->save($thumbPath);

				$image[] = array(
					'width' => $width,
					'height' => $height,
					'postfix' => $postfix,
					'name' => $name
				);
			}

			Image::make($movedPath)
					->resize($mainWidth, $mainHeight, $preserveRatio, $upsizeThumbs)
					->save($imageFolder . '/' . $nameFull);

			$return[] = array(
				'folder' => $folder,
				'originalName' => $in->getClientOriginalName(),
				'originalNewName' => $nameFull,
				'extension' => $ext,
				'thumbs' => $image,
				'thumbFolder' => $options['thumbsFolder']
			);
		}
		
		return $return;
	}

	protected function createDirIfNotExists($dir)
	{
		if (!file_exists($dir)) {
			return mkdir($dir);
		}
	}


}