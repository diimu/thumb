<?php namespace Diimu\Thumb;

use Intervention\Image\Image;

class Thumb
{
	/**
	 * the data need to be in this way array(path, type(video, image))
	 * @var array
	 */
	protected $config;
	protected $images = array();

	public function __construct($config)
	{
		$this->config = $config;
	}

	public function add($path, $type="")
	{
		if(is_array($path))
		{
			$this->addMediaArray($path);
		}
		elseif(is_object($path))
		{
			$this->addInputClass($path);
		}
		else
		{
			$this->addMedia($path, $type);
		}

		return $this;
	}

	public function object($object)
	{
		return $this->addInputClass($object);
	}

	protected function addMedia($media, $type)
	{
		if ($type == "image")
		{
			$this->addImage($media);
		}
	}

	protected function addMediaArray($medias)
	{
		foreach($medias as $path => $type)
		{
			$this->addMedia($path, $type);
		}
	}

	protected function addInputClass($class)
	{
		$media = new ThumbInput($this->config);
		$media->addInput($class);
		return $media;
	}

	protected function addImage($image)
	{
		$this->images[] = $image;
	}

	public function process($options=false)
	{
		$options = $this->loadOptions($options);

		return $this->processImage($options);
	}

	protected function loadOptions($options=false)
	{
		return ($options) ? $this->setOptions($options) : $this->setOptions();
	}

	protected function setOptions($merge=false)
	{
		return ($merge) ? array_merge($this->getConfig('defaults'), $merge) : $this->getConfig('defaults');
	}

	protected function processImage($options)
	{
		echo print_r($this->images);
	}


	//test functions
	protected function setConfig($config, $value)
	{
		$this->config->set('thumb::'.$config, $value);
	}

	protected function getConfig($config)
	{
		return $this->config->get('thumb::'.$config);
	}

	public function setUploadPath($path)
	{
		$this->setConfig('uploadPath', $path);
	}

	public function loadConfigItem()
	{
		return $this->getConfig('uploadPath');
	}

	public function checkExtensionIsWorking()
	{
		return (new Image()) ? true : false;
	}

	public function countImages()
	{
		return count($this->images);
	}

	public function getImageAt($slice)
	{
		$images = $this->images;
		return $images[$slice];
	}
}
