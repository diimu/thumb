<?php
namespace Diimu\Thumb\Test;

use Diimu\Thumb\Media as Media;
use Orchestra\Testbench\TestCase;
use PHPUnit_Framework_TestCase;

class MainTest extends PHPUnit_Framework_TestCase {

	public function testAddingImageSingleCount()
	{
		$media = new Media();
		$media->add('path', 'image');
		$this->assertEquals(1, $media->countImages());
	}

	public function testAddingImageTwiceBySingleCount()
	{
		$media = Media::add('path', 'image')->add('path2', 'image')->add('path3', 'image');
		$media = new Media();
		$media->add('path', 'image')->other('path2', 'image')->other('path3', 'image');
		$this->assertEquals(3, $media->countImages());
	}


	public function testCheckingFirstImage()
	{
		$media = Media::add('path', 'image');
		$media = new Media();
		$media->add('path', 'image');
		$this->assertSame('path', $media->getImageAt(0));
	}

	public function testAddindImageMultipleCount()
	{
		$images = array('path1' => 'image', 'path2' => 'image');

		$media = Media::add($images);
		$media = new Media(); 
		$media->add($images);
		$this->assertEquals(2, $media->countImages());
	}

	public function testProcessingImageSingleVariousMethods()
	{
		$media = Media::add('path', 'image');
		$media = new Media();
		$media->add('path', 'image');

		$options = array(
			'resize' => array(
				array(
					"size" => "180x110",
					"name" => 'small'
				),
				array(
					"w" => 250,
					"h" => 350,
					"name" => 'medium'
				),
			)
		);

		$process = $media->process($options);
		$this->assertEquals(1, $media->countImages());
	}

	public function testImageExtensionWorking()
	{
		$this->assertTrue(Media::checkExtensionWorking());
	}

	public function testConfigLoad()
	{
		echo Media::loadConfigItem();
	}
		$media = new Media();
		$this->assertTrue($media->checkExtensionWorking());
	}

	/*public function testConfigLoad()
	{
		$media = new Media();
		echo $media->loadConfigItem();
	}*/
}